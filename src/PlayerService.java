
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author PC
 */
public class PlayerService {

    static Player O, X;

    static {
        O = new Player('O');
        X = new Player('X');

    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {
            file = new File("ox.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            O = (Player) ois.readObject();
            X = (Player) ois.readObject();
           
        } catch (FileNotFoundException ex) {
            
        } catch (IOException ex) {
            
        } catch (ClassNotFoundException ex) {
           
        }
    System.out.println(O);
    System.out.println(X);
    }
    

    public static void save() {
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        File file = null;
        try {

            file = new File("ox.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(O);
            oos.writeObject(X);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFlie.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFlie.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                if (oos != null) {
                    oos.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException ex) {
                Logger.getLogger(WriteFlie.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static Player getO() {
        return O;
    }

    public static Player getX() {
        return X;
    }

}
